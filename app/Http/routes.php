<?php

Route::get('/', [
  'uses' =>'\LearningProject\Http\Controllers\HomeController@index',
  'as' =>'home',
]);

Route::get('/alert', function() {
  return redirect()->route('home')->with('info', 'You have signed up!');
});
/**
  * Authentication
  */
  Route::get('/signup', [
    'uses' => '\LearningProject\Http\Controllers\AuthController@getSignup',
    'as' => 'auth.signup',
    'middleware' => ['guest'],
  ]);

  Route::post('/signup', [
    'uses' => '\LearningProject\Http\Controllers\AuthController@postSignup',
    'middleware' => ['guest'],
  ]);
  Route::get('/signin', [
    'uses' => '\LearningProject\Http\Controllers\AuthController@getSignin',
    'as' => 'auth.signin',
    'middleware' => ['guest'],
  ]);

  Route::post('/signin', [
    'uses' => '\LearningProject\Http\Controllers\AuthController@postSignin',
    'middleware' => ['guest'],
  ]);
  Route::get('/signout', [
    'uses' => '\LearningProject\Http\Controllers\AuthController@getSignout',
    'as' => 'auth.signout',
  ]);
/**
  * Search
  */
  Route::get('/search', [
    'uses' => '\LearningProject\Http\Controllers\SearchController@getResults',
    'as' => 'search.results',
  ]);
/**
 * User Profile
 */
 Route::get('/user/{username}', [
   'uses' => '\LearningProject\Http\Controllers\ProfileController@getProfile',
   'as' => 'profile.index',
 ]);
Route::get('/profile/edit',[
  'uses' => '\LearningProject\Http\Controllers\ProfileController@getEdit',
  'as' => 'profile.edit',
  'middleware' => ['auth'],
]);
Route::post('/profile/edit',[
  'uses' => '\LearningProject\Http\Controllers\ProfileController@postEdit',
  'middleware' => ['auth'],
]);
Route::get('/friends',[
  'uses' => '\LearningProject\Http\Controllers\FriendController@getIndex',
  'as' => 'friend.index',
  'middleware' => ['auth'],
]);
Route::get('/friends/add/{username}',[
  'uses' => '\LearningProject\Http\Controllers\FriendController@getAdd',
  'as' => 'friend.add',
  'middleware' => ['auth'],
]);
Route::get('/friends/accept/{username}',[
  'uses' => '\LearningProject\Http\Controllers\FriendController@getAccept',
  'as' => 'friend.accept',
  'middleware' => ['auth'],
]);
